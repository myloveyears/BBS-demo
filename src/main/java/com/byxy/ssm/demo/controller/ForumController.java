package com.byxy.ssm.demo.controller;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.byxy.ssm.demo.entity.Forum;
import com.byxy.ssm.demo.entity.Tab;
import com.byxy.ssm.demo.entity.Tip;
import com.byxy.ssm.demo.service.MainService;
//import com.byxy.ssm.demo.service.TabService;
import com.byxy.ssm.demo.service.TabService;
import com.byxy.ssm.demo.service.TipService;

import lombok.extern.slf4j.Slf4j;

import com.byxy.ssm.demo.service.ForumService;

@Controller
@Slf4j
public class ForumController {
	

	@Autowired
	private ForumService forumService;
	@Autowired
	private TabService tabService;
	@GetMapping("toPublishTipPage.do")
	public String tipadd(Model model) {
		List<Forum> forums = forumService.findAll();
		model.addAttribute("forums", forums);
		
		
		
		List<Tab> tabs = tabService.tabfindAll();
		model.addAttribute("tabs", tabs);
		return "publishTip";
	}
	
	
}
