package com.byxy.ssm.demo.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.byxy.ssm.demo.entity.Reply;
import com.byxy.ssm.demo.entity.Tip;
import com.byxy.ssm.demo.entity.User;
import com.byxy.ssm.demo.service.ForumService;
import com.byxy.ssm.demo.service.TabService;
import com.byxy.ssm.demo.service.TipService;
import com.byxy.ssm.demo.service.ReplyService;
import com.byxy.ssm.demo.vo.TipVO;

import lombok.extern.slf4j.Slf4j;
@Controller
@Slf4j
public class TipController {
	
	private Logger logger = Logger.getLogger(getClass());
	@Autowired
	private ForumService forumService;
	@Autowired
	private TabService tabService;
	@Autowired
	private TipService tipService;
	
	@Autowired
	private ReplyService replyService;
	@RequiresPermissions(value = { "user:d", "user:b" }, logical = Logical.OR)
	@PostMapping("publishNewTip.do")
	public String tipFindAll(Tip tip, String selectedForumId,HttpSession session) {
		
		logger.info("tipTitle"+tip.getTipTitle());
		logger.info("tipContent"+tip.getTipContent());
		logger.info("tabId"+tip.getTabId());
		logger.info("selectedForumId"+selectedForumId);
		User u = (User)session.getAttribute("USER");
		tip.setUserId(u.getUserId());
		tip.setTipClick(0);
		tip.setTipIsdeleted(1);
		tip.setTipIsKnot(2);
		tip.setTipModifyTime(new Date());
		tip.setTipPublishTime(new Date());
		tip.setTipReplies(3);
		tipService.add(tip);
		Subject subject = SecurityUtils.getSubject();
		subject.checkPermission("user:d");
		return "redirect:toMainPage.do";
	}
	@GetMapping("showTip.do")
	public String showTip(Integer tipId,Model model){
		logger.info("tipId" + "=" +tipId);
		TipVO tipVo = tipService.findById(tipId);
		model.addAttribute("tip", tipVo);
		List<Reply> replies = replyService.findByTip(tipId);
		model.addAttribute("replies", replies);
		return "tipContent";
	}
	//shiro安全权限管理会员权限
//	@RequiresPermissions(value = { "user:d", "user:b" }, logical = Logical.OR)
//	@GetMapping("toPublishTipPage.do")
//	public String add() {
//		Subject subject = SecurityUtils.getSubject();
//		subject.checkPermission("user:d");
//		return "publishTip";
//	}
	
}
