package com.byxy.ssm.demo.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.byxy.ssm.demo.service.MainService;
import com.byxy.ssm.demo.vo.TipVO;

@Controller
public class MainController {
	@Autowired
	private MainService mainService;
	@GetMapping("toMainPage.do")
	public String main(Model model) {
//		List<TipVO> tip = new ArrayList<>();
//		TipVO vo = new TipVO();
//		vo.setTipId(1);
//		vo.setTipClick(76);
//		vo.setTapName("其他");
//		vo.setTipModifyTime(new Date());
//		vo.setTipPublishTime(new Date());
//		vo.setTipReplies(6);
//		vo.setTipTitle("帖子测试");
//		vo.setUserName("user");
//		vo.setTipIsdeleted(1);
//		vo.setTipIsKnot(1);
//		tip.add(vo);
		List<TipVO> tips = mainService.find();
		model.addAttribute("tips", tips);
		return "main";
	}
}
