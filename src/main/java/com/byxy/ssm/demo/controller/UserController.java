package com.byxy.ssm.demo.controller;

import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.byxy.ssm.demo.entity.User;
import com.byxy.ssm.demo.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/")
@Slf4j  //lombok
public class UserController {
		// 1. 显示页面 
		 // 2. 定义业务类与方法
		 // 3. 定义Dao接口
		 // 4. 单元测试业务接口
		 // 5. 控制层与业务类整合
		 // 6. UI层与控制整合
	@Autowired
	private UserService userService;
	@GetMapping("toLoginPage.do")
	public String login(String error, Model model){
		if (error != null) {
			model.addAttribute("error", "error");
		}
		return "login";
	}
	
	@PostMapping("userLogin.do")
	public String dologin(String userName,String userPassword,String tipId,HttpSession session,Model model){
		// 1 创建Subject
				Subject subject = SecurityUtils.getSubject();
				// 2. 创建Token
				UsernamePasswordToken token = new UsernamePasswordToken(userName, userPassword);
				try {
					// 3.登录方法
					subject.login(token);
					User user = (User) subject.getPrincipal();
					session.setAttribute("USER", user);
				} catch (UnknownAccountException e) {
					model.addAttribute("error", "用户不存在");
					return "redirect:toLoginPage.do?error=error";
				} catch (IncorrectCredentialsException e) {
					model.addAttribute("error", "密码不正确");
					return "redirect:toLoginPage.do?error=error";
				}

				return "redirect:toMainPage.do";
			}
		//User 使用VO类代替，防止密码泄漏
//		User u = userService.getByNameAndPassword(userName,userPassword);
//		log.info("userName"+userName);
//		log.info("userPassword"+userPassword);
//		log.info("user="+u);
//		if(u==null){
//			return"redirect:userLogin.do";
//		}else{
//			u.setUserPassword("");
//			session.setAttribute("USER", u);
//			return "redirect:toMainPage.do";
//		}
//	}
	@GetMapping("userSignOut.do")
	public String logout(HttpSession session) {
		session.removeAttribute("USER");
		Subject subject = SecurityUtils.getSubject();
		subject.logout();
		return "redirect:toMainPage.do";
	}
	
	@GetMapping("toSignUpPage.do")
	public String reg(String error, Model model) {
		if (error != null) {
			model.addAttribute("error", "用户名已存在注册失败");
		}
		return "signUp";
	}
		
	@PostMapping("userSignUp.do")
	public String doReg(String userName,String userNick,String userPassword,Integer userType) {
		log.info("userName"+userName);
		log.info("userNick"+userNick);
		log.info("userPassword"+userPassword);
		log.info("userType"+userType);
		userService.save(userName, userNick, userPassword, userType);
		return "redirect:toLoginPage.do";
		//redirect:toLoginPage.do
	}
	
}
