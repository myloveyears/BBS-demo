package com.byxy.ssm.demo.vo;

import java.util.Date;

import lombok.Data;

@Data
public class TipVO {
		//贴子id
			private Integer tipId;
			//楼主id
			// private User user;
			private String userName;
			
			private Integer userId;
			//小分类id
			// private Tab tab;
			private String tabName;
			/*	版块名 */
			private String forumName;
			//标题 
			private String tipTitle;
			//发表时间 
			private Date tipPublishTime;
			// 更新时间
			private Date tipModifyTime;
			//贴子点击量
			private Integer tipClick;
			//是否逻辑删除，0否，1是
			private Integer tipIsdeleted;
			// 是否结贴，0否，1结贴 
			private Integer tipIsKnot;
			//贴子回复数 
			private Integer tipReplies;
			private String tipContent;
}
