package com.byxy.ssm.demo.entity;

import lombok.Data;

/**
 * 分类
 */
@Data
public class Tab {
	// 小分类id
	private Integer tabId;
	//小分类名
	private String tabName;
	// 版块id
	private Integer forumId;
	// 分类状态，0正常，1逻辑删除 
	private Integer tabStatus;
}