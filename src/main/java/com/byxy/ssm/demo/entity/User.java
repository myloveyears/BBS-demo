package com.byxy.ssm.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
	//id
	private Integer userId;
    // 用户名
    private String userName;
    //用户名称
    private String userNick;
    //密码
    private String userPassword;
    // 状态，0正常，1禁用，2锁定
    private Integer userStatus;
    //用户类型，0超级管理员，1，管理员，2普通用户
    private Integer userType;
	
    
    
}