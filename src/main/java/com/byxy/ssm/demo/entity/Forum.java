package com.byxy.ssm.demo.entity;

import lombok.Data;
//lombok
@Data
public class Forum {
	// 版块id
	private Integer forumId;
	// 版块名
	private String forumName;
	// 版块状态，0正常，1逻辑删除 
	// 可以用枚举类型,高级用法
	private Integer forumStatus;
}