package com.byxy.ssm.demo.entity;

import java.util.Date;

import lombok.Data;

/**
 * 回复表
 */
@Data
public class Reply {
   //回复id
    private Integer replyId;
    //回复人id
   // private User user;
    private Integer userId;
    //被回复贴id
   // private Tip tip;
    private Integer tipId;
    //回复发表时间
    private Date replyPublishTime;
    //回复修改时间
    private Date replyModifyTime;
    //回复状态，0正常，1逻辑删除
    private Integer replyStatus;
    //回复内容
    private String replyContent;
}
