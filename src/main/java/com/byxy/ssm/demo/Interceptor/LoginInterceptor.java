package com.byxy.ssm.demo.Interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.druid.support.logging.Log;
import com.byxy.ssm.demo.entity.User;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class LoginInterceptor implements HandlerInterceptor{
	//进入controller之前
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info("++++++preHandle");
		User u = (User) request.getSession().getAttribute("USER");
		if(u == null){
			response.sendRedirect("index.jsp");
			return false;
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}
	//退出controller之前
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		log.info("++++++postHandle");
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
	}
	//结束controller之前
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		log.info("++++++afterCompletion");
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
	}
	
}
