package com.byxy.ssm.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.byxy.ssm.demo.entity.Forum;
import com.byxy.ssm.demo.entity.Tip;
import com.byxy.ssm.demo.vo.TipVO;

public interface TipDao {
	

	@Results( id="tipMap",value=
			{
			@Result(column="tip_id",property="tipId",id=true),
			@Result(column="user_id",property="userId"),
			@Result(column="tab_id",property="tabId"),
			@Result(column="tip_title",property="tipTitle"),
			@Result(column="tip_content",property="tipContent"),
			@Result(column="tip_publishTime",property="tipPublishTime"),
			@Result(column="tip_modifyTime",property="tipModifyTime"),
			@Result(column="tip_click",property="tipClick"),
			@Result(column="tip_isDeleted",property="tipIsdeleted"),
			@Result(column="tip_isKnot",property="tipIsKnot"),
			@Result(column="tip_replies",property="tipReplies")
			}
	)
	@Select("select * from tip")
	List<Tip> find();
	
	@Insert("insert into tip "
			+ "(tip_id,user_id,tab_id,tip_title,tip_content,tip_publishTime,tip_modifyTime,tip_click,tip_isDeleted,tip_isKnot,tip_replies) values "
			+ "(#{tipId},#{userId},#{tabId},#{tipTitle},#{tipContent},#{tipPublishTime},#{tipModifyTime},#{tipClick},#{tipIsdeleted},#{tipIsKnot},#{tipReplies})")
	void add(Tip tip);
	
	@ResultMap("tipMap")
	@Select("select * from tip where tip_id = #{tipId}")
	Tip findById(Integer tipId);
}
