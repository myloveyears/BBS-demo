package com.byxy.ssm.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.byxy.ssm.demo.entity.Tab;



public interface TabDao {
	@Results( id="forum",value=
			{
			@Result(column="tab_id",property="tabId",id=true),
			@Result(column="tab_name",property="tabName"),
			@Result(column="forum_id",property="forumId"),
			@Result(column="tab_status",property="tabStatus")
			}
	)
	@Select("select * from tab where tab_id=#{tab_id}")
	Tab getById(Integer tabId);
	
	@ResultMap("forum")
	@Select("select * from tab")
	List<Tab> tabfind();
	
}
