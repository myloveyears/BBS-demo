package com.byxy.ssm.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.byxy.ssm.demo.entity.Forum;
import com.byxy.ssm.demo.entity.Tip;
import com.byxy.ssm.demo.entity.User;

public interface ForumDao {
	@Results( id="forum",value=
			{
			@Result(column="forum_id",property="forumId",id=true),
			@Result(column="forum_name",property="forumName"),
			@Result(column="forum_status",property="forumStatus")
			}
	)
	@Select("select * from forum where forum_id=#{forumId}")
	Forum getById(Integer forumId);
	
	@ResultMap("forum")
	@Select("select * from forum")
	List<Forum> find();
}
