package com.byxy.ssm.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.byxy.ssm.demo.vo.TipVO;

public interface TabVoDao {

	@Select("SELECT t.tip_id tipId,f.forum_name forumName, u.user_name userName, t.user_id userId ,tb.tab_name tabName,t.tip_title tipTitle, t.tip_publishTime tipPublishTime ,t.tip_modifyTime tipModifyTime, t.tip_click tipClick, t.tip_isDeleted tipIsdeleted,t.tip_isKnot tipIsKnot, t.tip_replies tipReplies " + 
			"FROM tip t LEFT JOIN user u ON u.user_id = t.user_id " + 
			"LEFT JOIN tab tb ON tb.tab_id = t.tab_id " + 
			"LEFT JOIN forum f ON f.forum_id = tb.forum_id")
	List<TipVO> find();

}
