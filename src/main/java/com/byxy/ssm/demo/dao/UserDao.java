package com.byxy.ssm.demo.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.byxy.ssm.demo.entity.User;

public interface UserDao {
	@Results(id = "usermap", value = {
		@Result(column = "user_id",property = "userId",id = true),
		@Result(column = "user_name",property = "userName"),
		@Result(column = "user_nick",property = "userNick"),
		@Result(column = "user_password",property = "userPassword"),
		@Result(column = "user_status",property = "userStatus"),
		@Result(column = "user_type",property = "userType")
	})
		@Select("select * from user where user_id=#{userId}")
		User getById(Integer userId);
	
	@ResultMap("usermap")
	@Select("select * from user where user_name=#{userName} and user_password=#{userPassword}")
	User getByNameAndPassword(@Param("userName") String userName,@Param("userPassword") String userPassword);
	
	@Insert("insert into user(user_name,user_nick,user_password,user_type) values(#{userName},#{userNick},#{userPassword},#{userType})")
	void save(User u);

	@Select("select * from user where user_name=#{username}")
	@ResultMap("usermap")
	public User getByUsername(@Param("username")String username);

	

	
}
