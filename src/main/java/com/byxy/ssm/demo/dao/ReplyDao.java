package com.byxy.ssm.demo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.byxy.ssm.demo.entity.Reply;

public interface ReplyDao {
	
	@Results(id="rep",value={
			@Result(column="reply_id",property="replyId",id=true),
			@Result(column="user_id",property="userId"),
			@Result(column="tip_id",property="tipId"),
			@Result(column="reply_content",property="replyContent"),
			@Result(column="reply_publishTime",property="replyPublishTime"),
			@Result(column="reply_modifyTime",property="replyModifyTime"),
			@Result(column="reply_status",property="replyStatus")
			
		})
	@Select("select * from reply where tip_id = #{tipId}")
	List<Reply> findByTipId(Integer tipId);

}
