package com.byxy.ssm.demo.service;

import com.byxy.ssm.demo.entity.User;

public interface UserService {
	
	/**
	 * 
	 * @param userName
	 * @param userPassword
	 * @return 找到返回页面 找不到返回null
	 */
	User getByNameAndPassword(String userName,String userPassword);
	void save(String user_name, String user_nick, String user_password, Integer user_type);
	User loginByName(String username);
	
}
