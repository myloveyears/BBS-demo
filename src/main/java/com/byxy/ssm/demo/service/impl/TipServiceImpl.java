package com.byxy.ssm.demo.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.byxy.ssm.demo.dao.ForumDao;
import com.byxy.ssm.demo.dao.TabDao;
import com.byxy.ssm.demo.dao.TipDao;
import com.byxy.ssm.demo.dao.UserDao;
import com.byxy.ssm.demo.entity.Forum;
import com.byxy.ssm.demo.entity.Tab;
import com.byxy.ssm.demo.entity.Tip;
import com.byxy.ssm.demo.entity.User;
import com.byxy.ssm.demo.service.TipService;
import com.byxy.ssm.demo.vo.TipVO;
@Service
@Transactional
public class TipServiceImpl implements TipService{
		
		@Autowired
		private TipDao tipDao;
		@Autowired
		private UserDao userDao;
		@Autowired
		private ForumDao forumDao;
		@Autowired
		private TabDao tabDao;
		@Override
		public void add(Tip tip) {
			tipDao.add(tip);
	}
		@Override
		public TipVO findById(Integer tipId) {
			TipVO tipvo = new TipVO();
			Tip tip = tipDao.findById(tipId);
			BeanUtils.copyProperties(tip, tipvo);
			User u = userDao.getById(tip.getUserId());
			if(u.getUserNick()!=null && u.getUserNick().equals(""))
				tipvo.setUserName(u.getUserNick());
			else
				tipvo.setUserName(u.getUserName());
			//vo.setTipId(p.getTabId());
			Tab t = tabDao.getById(tip.getTabId());
			Forum f = forumDao.getById(t.getForumId());
			if(f != null) {
				tipvo.setForumName(f.getForumName());
			}
			if(t != null) {
				tipvo.setTabName(t.getTabName());
			}
			
			
			return tipvo;
		}
	

		
}
