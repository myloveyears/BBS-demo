package com.byxy.ssm.demo.service.impl;

import java.util.List;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.byxy.ssm.demo.dao.ForumDao;
import com.byxy.ssm.demo.entity.Forum;
import com.byxy.ssm.demo.service.ForumService;
@Service
@Transactional
public class ForumServiceImpl implements ForumService {
	@Resource
	private ForumDao forumDao;
	public List<Forum> findAll(){
		return forumDao.find();
	}
}
