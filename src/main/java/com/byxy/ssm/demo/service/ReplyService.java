package com.byxy.ssm.demo.service;

import java.util.List;

import com.byxy.ssm.demo.entity.Reply;

public interface ReplyService {

	
	List<Reply> findByTip(Integer tipId);
}
