package com.byxy.ssm.demo.service;

import com.byxy.ssm.demo.entity.Tip;
import com.byxy.ssm.demo.vo.TipVO;

public interface TipService {

	void add(Tip tip);

	TipVO findById(Integer tipId);

}
