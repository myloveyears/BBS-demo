package com.byxy.ssm.demo.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.byxy.ssm.demo.dao.ForumDao;
import com.byxy.ssm.demo.dao.TabDao;
import com.byxy.ssm.demo.dao.TabVoDao;
import com.byxy.ssm.demo.dao.TipDao;
import com.byxy.ssm.demo.dao.UserDao;
import com.byxy.ssm.demo.entity.Forum;
import com.byxy.ssm.demo.entity.Tab;
import com.byxy.ssm.demo.entity.Tip;
import com.byxy.ssm.demo.entity.User;
import com.byxy.ssm.demo.service.MainService;
import com.byxy.ssm.demo.vo.TipVO;

@Service
@Transactional
public class MainServiceImpl implements MainService{
	@Autowired
	private TipDao tipDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private TabDao tabDao;
	@Autowired
	private ForumDao forumDao;
	
	@Autowired
	private TabVoDao tabVodao;
	@Override
	public List<TipVO> find() {
		List<Tip> tips = tipDao.find();
		List<TipVO> tipvos = new ArrayList<>();
		
		///// 组合
		for(Tip p : tips) {
			TipVO vo = new TipVO();
			BeanUtils.copyProperties(p, vo);
			User u = userDao.getById(p.getUserId());
			if(u.getUserNick()!=null && u.getUserNick().equals(""))
				vo.setUserName(u.getUserNick());
			else
				vo.setUserName(u.getUserName());
			//vo.setTipId(p.getTabId());
			Tab t = tabDao.getById(p.getTabId());
			Forum f = forumDao.getById(t.getForumId());
			if(f != null) {
				vo.setForumName(f.getForumName());
			}
			if(t != null) {
				vo.setTabName(t.getTabName());
			}
			
			tipvos.add(vo);
		}
		
		return tipvos;
	}
	@Override
	public List<TipVO> findByVO() {
		//	SQL脚本拼装数据
		return tabVodao.find();
	}
}
