package com.byxy.ssm.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.byxy.ssm.demo.dao.UserDao;
import com.byxy.ssm.demo.entity.User;
import com.byxy.ssm.demo.service.UserService;



@Service
@Transactional
public class UserServiceImpl implements UserService{
	@Autowired
	private UserDao userDao;
	@Override
	public User getByNameAndPassword(String userName,String userPassword) {
		
		return userDao.getByNameAndPassword(userName, userPassword);
	}
	
	@Override
	public void save(String user_name, String user_nick, String user_password, Integer user_type) {
		// TODO Auto-generated method stub
		User u = new User();
		u.setUserName(user_name);
		u.setUserNick(user_nick);
		u.setUserPassword(user_password);
		u.setUserType(user_type);
		userDao.save(u);
	}

	@Override
	public User loginByName(String username) {
		// TODO Auto-generated method stub
		return userDao.getByUsername(username);
	}

	
	
}
