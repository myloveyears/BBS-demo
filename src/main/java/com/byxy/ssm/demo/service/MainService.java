package com.byxy.ssm.demo.service;

import java.util.List;

import com.byxy.ssm.demo.vo.TipVO;

public interface MainService {

	List<TipVO> find();

	List<TipVO> findByVO();

}
