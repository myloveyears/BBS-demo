package com.byxy.ssm.demo.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.byxy.ssm.demo.dao.ForumDao;
import com.byxy.ssm.demo.dao.TabDao;
import com.byxy.ssm.demo.entity.Forum;
import com.byxy.ssm.demo.entity.Tab;
import com.byxy.ssm.demo.service.TabService;

@Service
@Transactional
public class TabServiceImpl implements TabService{
	@Autowired
	private TabDao tabumDao;
	public List<Tab> tabfindAll() {
		// TODO Auto-generated method stub
		return tabumDao.tabfind();
	}
	
}
