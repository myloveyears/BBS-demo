package com.byxy.ssm.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.byxy.ssm.demo.dao.ReplyDao;
import com.byxy.ssm.demo.entity.Reply;
import com.byxy.ssm.demo.service.ReplyService;

@Service
@Transactional
public class ReplyServiceImpl implements ReplyService {
	
	@Autowired
	private ReplyDao replyDao;
	@Override
	public List<Reply> findByTip(Integer tipId) {
		// TODO Auto-generated method stub
		return replyDao.findByTipId(tipId);
	}

}
