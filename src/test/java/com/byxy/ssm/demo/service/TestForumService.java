package com.byxy.ssm.demo.service;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration({"classpath:spring-beans.xml"})
public class TestForumService {

	
	@Autowired
	private ForumService forumService;
	@Test
	public void testFind() {
		forumService.findAll().forEach(System.out::println);
		
	}
}
