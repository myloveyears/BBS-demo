package com.byxy.ssm.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.byxy.ssm.demo.service.TipService;
import com.byxy.ssm.demo.vo.TipVO;

@RunWith(SpringRunner.class)
@ContextConfiguration({"classpath:spring-beans.xml"})
public class TestTipService {

	@Autowired
	private  TipService tipService;
	
	@Test
	public void testfindById() {
		TipVO vo = tipService.findById(3);
		System.out.println(vo);
	}
}
