package com.byxy.ssm.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration({"classpath:spring-beans.xml"})
public class TestReplyService {

	@Autowired
	private  ReplyService tabService;
	@Autowired
	private  MainService mainService;
	
	@Test
	public void testfind() {
		
		tabService.findByTip(3).forEach(System.out::println); // java8 
	}
	
	@Test
	public void testfind2() {
		
		mainService.find().forEach(System.out::println); // java8 
	}
}
