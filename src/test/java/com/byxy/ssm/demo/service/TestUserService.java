package com.byxy.ssm.demo.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.byxy.ssm.demo.entity.User;

@RunWith(SpringRunner.class)
@ContextConfiguration({"classpath:spring-beans.xml"})
public class TestUserService {

 @Autowired
 private UserService userService;
 
 @Test
 public void testLogin() {
  String userName="xy";
  String userPassword ="123456";
  User u = userService.getByNameAndPassword(userName, userPassword);
  System.out.println(u);
 }
 
 @Test
 public void testSave() {
//   INFO [http-nio-8080-exec-10] - user_name = 123456
//     INFO [http-nio-8080-exec-10] - user_nick = abc
//     INFO [http-nio-8080-exec-10] - user_password = 098776
//     INFO [http-nio-8080-exec-10] - user_type = 2
  String user_password="098776";
  String user_nick ="abc";
  Integer user_type = 2;
  String user_name ="123456";
  userService.save(user_name,user_nick,user_password,user_type);
 }

}
