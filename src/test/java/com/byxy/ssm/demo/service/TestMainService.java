package com.byxy.ssm.demo.service;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.byxy.ssm.Tests;
import com.byxy.ssm.demo.vo.TipVO;

public class TestMainService extends Tests{
	@Autowired
	private MainService mainService;
	
	@Test
	public void testFind() {
		List<TipVO> tipVos = mainService.find();
		for(TipVO vo : tipVos) {
			System.out.println(vo);
		}
	}
}
